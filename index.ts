import { ApolloServer } from 'apollo-server'
import { AppModule } from './src/graphql/app.module'
import { buildSchemaSync } from 'type-graphql'
import ServerConfig from './src/config/server.config'

ServerConfig.loadOrm().then(() => {
  const resolvers = [...AppModule]
  const schema = buildSchemaSync({
    resolvers,
  });
  const server = new ApolloServer({
    schema,
    context: ServerConfig.graphqlContext,
    subscriptions: {
      onConnect: (connectionParams, websocket, context) => {

      },
      onDisconnect: (websocket, context) => {

      },
    },
  })
  server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`)
  })
});

