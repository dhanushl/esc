
import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from 'typeorm'

@Entity()
export class Event extends   BaseEntity{

  @PrimaryGeneratedColumn()
  id: number =0;
  @Column()
  name: string ="";

  @Column()
  city: string ="";

  @Column()
  timezone: string="";

  @Column()
  startDate: string ="";

  @Column()
  endDate: string ="";

  @Column()
  email: string ="";

  @Column()
  supportEmail: string="";

  @Column()
  bannerImage: string ="";
}
