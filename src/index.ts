import 'reflect-metadata'
import { createConnection } from 'typeorm'
import { ApolloServer } from 'apollo-server'
import { AppModule } from './graphql/app.module'
import { buildSchemaSync } from 'type-graphql'
import ServerConfig from './config/server.config'

createConnection().then(async connection => {
  let resolvers = [...AppModule];
  const schema = buildSchemaSync({
    resolvers: resolvers,
  });

  const graphqlServer = new ApolloServer({
    schema,
    context: ServerConfig.graphqlContext,
    subscriptions: {
      onConnect: (connectionParams, websocket, context) => {

      },
      onDisconnect: (websocket, context) => {

      },
    },
  });

  graphqlServer.listen().then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`)
  })
}).catch(error => console.log(error.message));
