import {MigrationInterface, QueryRunner, Table} from 'typeorm'

export class createEvent1583167762961 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        try {
            await queryRunner.createTable(new Table({
                name: 'event',
                columns: [
                    {
                        name: 'id',
                        type: 'int', //uuid
                        isPrimary: true,
                    },
                    {
                        name: 'name',
                        type: 'varchar',
                    },
                    {
                        name: 'city',
                        type: 'varchar',
                    },
                    {
                        name: 'timezone',
                        type: 'varchar',
                    },
                    {
                        name: 'startDate',
                        type: 'varchar',
                    },
                    {
                        name: 'endDate',
                        type: 'varchar',
                    },
                    {
                        name: 'email',
                        type: 'varchar',
                    },
                    {
                        name: 'supportEmail',
                        type: 'varchar',
                    },
                    {
                        name: 'bannerImage',
                        type: 'varchar',
                    },
                ],

            }), true);
        } catch (err) {
            //
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
