import dotenv from 'dotenv'
import path from 'path'

import { createConnection } from 'typeorm'

export default class ServerConfig {
  static appRoot: any
  static storage: any
  static logs: any

  public static async loadOrm() {
    // Load Basic
    this.loadSettings()
    await createConnection()
  }

  public static loadSettings() {
    // Load .env File
    dotenv.config()

    //Save App Root Path
    ServerConfig.appRoot = path.resolve(__dirname + '/../../') + '/'
    ServerConfig.storage = ServerConfig.appRoot + process.env.STORAGE
    ServerConfig.logs = ServerConfig.storage + '/logs/'
  }

  static async graphqlContext(context: any) {
    // make a request to portal api
    return {
      //Auth
      //Crypt
    }
  }
}
