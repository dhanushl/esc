
import {ArgsType, Field, ObjectType} from "type-graphql";
import {BaseEntity, Entity, PrimaryGeneratedColumn} from "typeorm";

@ObjectType()
export class EventData{

    @Field()
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    name: string;

    @Field()
    city: string;

    @Field()
    timezone: string;

    @Field()
    startDate: string;

    @Field()
    endDate: string;

    @Field()
    email: string;

    @Field()
    supportEmail: string;

    @Field()
    bannerImage: string;
}

@ArgsType()
export class IdDataArgs{

    @Field()
    id: number;
}


@ArgsType()
export  class EventDataArgs {
    @Field()
    id: number;

    @Field()
    name: string;

    @Field()
    city: string;

    @Field()
    timezone: string;

    @Field()
    startDate: string;

    @Field()
    endDate: string;

    @Field()
    email: string;

    @Field()
    supportEmail: string;

    @Field()
    bannerImage: string;
}

@ArgsType()
export class TestArgs{
    @Field()
    name: string;
    @Field()
    id: string;
}