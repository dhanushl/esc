
import {Arg, Args, Ctx, Mutation, Query, Resolver} from "type-graphql";
import {EventDataArgs, EventData, TestArgs, IdDataArgs} from "./object.type";
import {getConnection, Repository} from "typeorm";
import {InjectRepository} from "typeorm-typedi-extensions";
import {Event} from "../../entity/event";


@Resolver(EventData)
export default class EventResolver {

    // constructor(@InjectRepository(EventData) private readonly eventD: Repository<Event>) {
    // }

    @Query(returns => EventData)
    async getEventById(@Args() args: IdDataArgs) {
        console.log(args);
        const event:any = await Event.find({where: {id: args.id}});
        return event;
    }

    @Mutation(returns => EventData)
    async createEvent(@Args() args: EventDataArgs) {
        const data: any = Event.create(args);
        await data.save();
        return args;
    }

    @Mutation(returns => EventData)
    async updateEvent(@Args() args: EventDataArgs) {
        const updatedEvent: any = await Event.update(args.id, args);
        return args;
    }

    @Mutation(returns => EventData)
    async deleteEventById(@Args() args: IdDataArgs) {
         await Event.delete({id: args.id});
        return args;
    }


}