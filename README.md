# Awesome Project Build with TypeORM

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file
3. Run `npm start` command


## ENV
       #POSTGRES
       DB_USER='postgres'
       DB_HOST='localhost'
       DB_NAME='ecs'
       DB_PORT='5432'
       DB_PASSWORD='123'

## Migrate
    yarn migrate